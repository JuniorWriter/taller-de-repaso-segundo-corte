/* 5. Hacer un algoritmo que lea el nombre, la edad, el sexo (1 o 2) y el estado civil
de cualquier persona y muestre todos los datos solo sí la persona es soltera y mayor de edad*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	bool sexo;
    int edad,estado_civil;
    string genero,nombre;

	system("cls");
    cout<<"\t\t\t\t\t\t\tBIENVENIDO!\n\n\n";

	cout<<"Ingrese su nombre: ";
    cin>>nombre;
    cout<<"Ingrese la edad: ";
    cin>>edad;
    cout<<"Ingrese su sexo: ";
    cin>>sexo;
    cout<<"Escoga su estado civil - (1) Casado       (2) Soltero: ";
    cin>>estado_civil;

    if (sexo==true)
    {
        genero="Hombre";
    }
    else if (sexo==false)
    {
        genero="Mujer";
    }
    

    if ((edad>17)&&(estado_civil==2))
    {
        cout<<"\nSu nombre es "<<nombre<<", usted es un/a "<<genero<<", tiene "<<edad<<" a"<<char(164)<<"os y esta solter@\n\n";
    }
    

	system("pause");
	return 0;
}
