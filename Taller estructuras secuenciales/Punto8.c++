/*8. Un médico determina si una persona tiene anemia o puede padecer una
cardiopatía de acuerdo a su nivel de hemoglobina en la sangre, su edad y su
sexo. Si el nivel de hemoglobina que tiene una persona es menor que el rango
que le corresponde, se determina que el paciente sufre de anemia. En caso
contrario el paciente puede padecer algún tipo de cardiopatía y se le recomienda
realizarse otros exámenes. La tabla en la que el médico se basa para obtener el
diagnóstico es la siguiente:

Realice un algoritmo que, dada la edad, sexo y valor de la hemoglobina de un
paciente, determine si está sano, tiene anemia o es necesario realizar otros
exámenes.*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
	int sexo;
	float hemo,edad;

	system("color 0f");
	
	cout<<"\t\t\tNIVEL DE HEMOGLOBINA\n\n\n";

    cout<<"Escoga su sexo [1.Masculino   -   2.Femenino]:";
    cin>>sexo;
    cout<<"Ingrese su edad: ";
    cin>>edad;
    cout<<"Ingrese su nivel de hemoglobina: ";
    cin>>hemo;

    cout<<"\n\n";

    if ((edad<1)&&(hemo<13.5))
    {
        cout<<"El paciente sufre de anemia";
    }
    else if ((edad<1)&&(hemo>19.5))
    {
        cout<<"El paciente puede padecer de cardiopatia";
    }


    else if ((edad>=1)&&(edad<=5)&&(hemo<12))
    {
        cout<<"El paciente sufre de anemia";
    }
    else if ((edad>=1)&&(edad<=5)&&(hemo>14))
    {
        cout<<"El paciente puede padecer de cardiopatia";
    }


    else if ((edad>5)&&(edad<=15)&&(hemo<14))
    {
        cout<<"El paciente sufre de anemia";
    }
    else if ((edad>5)&&(edad<=15)&&(hemo>16))
    {
        cout<<"El paciente puede padecer de cardiopatia";
    }

    else if ((sexo==1)&&(edad>15)&&(hemo<13))
    {
        cout<<"El paciente sufre de anemia";
    }
    else if ((sexo==1)&&(edad>15)&&(hemo>16))
    {
        cout<<"El paciente puede padecer de cardiopatia";
    }

    else if ((sexo==2)&&(edad>15)&&(hemo<11.5))
    {
        cout<<"El paciente sufre de anemia";
    }
    else if ((sexo==2)&&(edad>15)&&(hemo>14.5))
    {
        cout<<"El paciente puede padecer de cardiopatia";
    }

	cout<<"\n";
	
	getch();
	return 0;
}
