/* Realizar un programa que calcule la conjetura de ULAM: Ingresar un
número entero positivo. Si es par se divide entre 2 y si es impar se
multiplica por 3 y se le suma 1. Imprimir todos los números generados hasta
llegar a 1. Por ejemplo si empezamos en 5 la serie sería: 5, 16, 8, 4, 2, 1.*/

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

using namespace std;

int main()
{
    int num=0;

    cout<<"\t\t\tCONJETURA DE ULAM\n\n";

    cout<<"Ingrese un numero: ";
    cin>>num;

    do
    {
        if(num%2==0)
        {
            num/=2;
        }
        else
        {
            num*=3;
            num+=1;
        }
        cout<<num<<endl;
        getch();
        
    } while (num!=1);
    




    system("pause");
    return 0;
}
